FROM openjdk:11
VOLUME /tmp

WORKDIR /app
ADD ${JAR_FILE} /app/
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/${JAR_FILE}"]