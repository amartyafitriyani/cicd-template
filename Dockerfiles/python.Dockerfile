FROM python:3.8-slim-buster

ARG APP_PY='app.py'
WORKDIR /app

# TODO COPY ALL APP tidak hanya app.py 
COPY ${APP_PY} /app/app.py

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
 
EXPOSE 8080

#CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
#CMD [ "python3", "app.py"]

ENTRYPOINT [ "python3" , "app.py" ]