#FROM
FROM eclipse-temurin:11-jdk-alpine
VOLUME /tmp
#ARGS
ARG JAVA_OPTS 
ARG JAR_FILE
#ARG PORT
#ENV
ENV JAVA_OPTS=$JAVA_OPTS
#flyway
RUN apk --no-cache add --update bash openssl

# WORKDIR /flyway

# ARG FLYWAY_VERSION
ENV FLYWAY_VERSION 8.5.9

RUN wget https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/${FLYWAY_VERSION}/flyway-commandline-${FLYWAY_VERSION}.tar.gz \
  && gzip -d flyway-commandline-${FLYWAY_VERSION}.tar.gz \
  && tar -xf flyway-commandline-${FLYWAY_VERSION}.tar --strip-components=1 \
  && rm flyway-commandline-${FLYWAY_VERSION}.tar

ENV PATH="/flyway:${PATH}"

# ENTRYPOINT ["flyway"]
# CMD ["-?"]
#COPY
COPY /target/$JAR_FILE app.jar
#EXPOSE
#EXPOSE $PORT
EXPOSE 8080
#ENTYRY
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar
#ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -Dserver.port=$PORT -jar app.jar

#ENV SPRING_PROFILES_ACTIVE=sit
#ENTRYPOINT [ "java", "-jar", "/app/target/demo-0.0.1-SNAPSHOT.jar", "--server.port=8081" ]
 
#ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom  -Dspring.profiles.active=sit -jar app.jar

# For Spring-Boot project, use the entrypoint below to reduce Tomcat startup time.
#ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar demo.jar
#target/demo-0.0.1-SNAPSHOT.jar

# jalankan command "cd .."  untuk keluar dari folder project ini #
# jalankan command "docker build -t trnbsm/fe_dashboard -f fe_dashboard/Dockerfile ." diluar folder /SandboxLDAP #
# kalau perlu rename tag bisa pakai ini "docker tag oldname/fe_dashboard:oldver trnbsm/fe_dashboard:latest #
# untuk deploy ke docker hub jalankan command ini " docker push trnbsm/fe_dashboard:latest"
# untuk run mannual docker run -p 9090:9090 trnbsm/fe_dashboard:latest -e ASPNETCORE_URLS=http://+:9090 -l fe_dashboard
#./mvnw package && java -jar local/user-management-service-0.1.0.jar -Dmaven.test.skip
#docker build -t local/user-management-service .