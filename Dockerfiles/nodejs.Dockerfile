FROM node:8.16.0-jessie
COPY ./build /app
RUN npm install http-server -g
WORKDIR /app


EXPOSE 8080
ENTRYPOINT ["http-server", "/app"]