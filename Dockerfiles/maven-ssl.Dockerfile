#FROM
FROM openjdk:11-jre-slim
VOLUME /tmp
#ARGS
ARG JAVA_OPTS 
ARG JAR_FILE
#ARG PORT
#ENV
ENV JAVA_OPTS=$JAVA_OPTS
#COPY
COPY /target/$JAR_FILE app.jar
RUN keytool \
    -keystore server.jks  -storepass protected  -deststoretype pkcs12 \
    -genkeypair -keyalg RSA -validity 395 -keysize 2048  -sigalg SHA256withRSA \
    -dname "CN=gateway-internal-dev.nobubank.com,O=IST,OU=IST,L=Bandung,ST=West Java,C=IDN" \
    -ext "SAN=IP:10.130.1.216,DNS:gateway-internal-dev.nobubank.com,EMAIL:email@ist.id"
RUN keytool \
    -certreq -keystore server.jks -storepass protected \
    -ext "SAN=IP:10.130.1.216,DNS:gateway-internal-dev.nobubank.com,EMAIL:email@ist.id" \
    -file domain.csr
#EXPOSE
#EXPOSE $PORT
EXPOSE 8080
#ENTYRY
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar
#ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -Dserver.port=$PORT -jar app.jar

