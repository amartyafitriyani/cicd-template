FROM node:14.16.0-alpine
# FROM node:nginx-114-rhel7
COPY ./build /app
RUN yarn global add serve
#RUN npm install http-server -g
WORKDIR /app


EXPOSE 8080
#ENTRYPOINT ["http-server", "app"]
ENTRYPOINT ["serve", "-s", ".", "-l","8080"]