###~PREVIOUS RUNNING DOCKERFILE~###
# Use a Node 14 base image
# FROM node:14.19.3-alpine as builder
# WORKDIR /app
# COPY ./build /app/build

# # Bundle static assets with nginx
# # FROM nginx:1.23.1-alpine as production
# # FROM twalter/openshift-nginx:latest as production
# # FROM registry.access.redhat.com/ubi8/nginx-120:1-60.1665590917 as production
# FROM nginx:1.23.1 as production
# COPY --from=builder /app/build /opt/app-root/src
# # COPY nginx.conf /etc/nginx/conf.d/nginx.conf
# COPY nginx.conf /opt/app-root/etc/nginx.d/nginx.conf
# RUN mkdir -p /var/cache/nginx/client_temp \
#              /var/cache/nginx/proxy_temp \
#              /var/cache/nginx/fastcgi_temp \
#              /var/cache/nginx/scgi_temp \
#              /var/cache/nginx/uwsgi_temp
# RUN chmod g+rwx /var/cache/nginx /var/run /var/log/nginx
# RUN sed -i.bak 's/listen\(.*\)80;/listen 8081;/' /etc/nginx/conf.d/default.conf
# # RUN sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf
# RUN sed -i.bak 's/^user/#user/' /etc/nginx/conf.d/nginx.conf
# EXPOSE 8080
# CMD ["nginx", "-g", "daemon off;"]
# ###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~###

# FROM centos/nginx-114-centos8
# COPY ./build /opt/app-root/src
# COPY ./nginx.conf /etc/nginx/conf.d/nginx.conf
# WORKDIR /opt/app-root/src
# EXPOSE 8080
# CMD ["nginx", "-g", "daemon off;"]

# FROM node:14.19.3-alpine as builder
# WORKDIR /app
# COPY ./build /app/build

# # Bundle static assets with nginx
# FROM registry.access.redhat.com/ubi8/nginx-120:1-60.1665590917 as production
# COPY --from=builder /app/build /opt/app-root/src
# # ${NGINX_CONFIGURATION_PATH} = /opt/app-root/etc/nginx.d 
# ADD ./nginx.conf "${NGINX_CONFIGURATION_PATH}"  
# EXPOSE 8080
# CMD ["nginx", "-g", "daemon off;"]

FROM node:14.19.3-alpine AS builder
WORKDIR /app
COPY ./build /app/build


FROM nginx:1.23.1 as production
WORKDIR /opt/app-root/src
COPY --from=builder /app/build /opt/app-root/src
COPY nginx.conf /etc/nginx/conf.d/nginx.conf
EXPOSE 8080
ENTRYPOINT ["nginx", "-g", "daemon off;"]
