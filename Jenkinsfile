import org.jenkinsci.plugins.pipeline.modeldefinition.Utils 

def buildNumber = env.BUILD_NUMBER as int
if (buildNumber > 1) milestone(buildNumber - 1)
milestone(buildNumber)

//GIT CHECKOUT
def git_credentials_id = scm.userRemoteConfigs[0].credentialsId
def git_repo = scm.userRemoteConfigs[0].url
def git_branch = scm.branches[0].name
def resultlog

//PREPARE
def nexus_credentials_id = 'nexus_bsm_id'
def nexus_base_url = 'http://nexus.besmart-universal.com'
def nexus_deps_repo = "$nexus_base_url/repository/mvn-nobu/"
def nexus_deploy_repo = "$nexus_base_url/repository/maven-nobu/"
def env_file = "nobu"
def nexus_cicd_repo = "cicd-projects"
def jenkins_local_repository  = "/var/jenkins_home/.m2/repository_nobu/"
def appFullVersion
def pomappName
def nexus_docker_url = "nexus.besmart-universal.com:8097"
def nexus_docker_repo = "digital-banking"
def nexus_docker_group = "docker-group"

// //ENVIRONMENT
// def ocp_project = 'nobu'
// def pull_secret = 'default-dockercfg-hm2p7'
// def public_route_prefix = ''
// def ocp_base_domain = 'infosys.id'

//OC BUILD
// def oc_login_url = "https://api.devs.infosys.id:6443"
// def oc_credentials_id = 'oc_credentials_gcp_id'
// def appName = 'workflow-management-service'
// def gitCommitId


// GKE Deploy
def gke_keyfile = 'nobu-ist.json'
def gke_node = 'dev-digitalbanking'
def gke_region = 'asia-southeast2'
def gke_project = 'nobu-ist'
def appName = 'workflow-management-service'
def gke_namespace = 'nobu'

def skipDockerBuild = false
def skipDockerPush = false

node {

    stage('Checkout'){ 
        git url: "${git_repo}", branch: "${git_branch}", credentialsId: "${git_credentials_id}"
        resultlog = sh(returnStdout: true, script: 'git log  -1 --pretty=%B')
        resultlog = resultlog.toLowerCase()
        env.M2_HOME = "/opt/maven"
        env.PATH="/opt/gcloud/bin:/opt/oc:${env.M2_HOME}/bin:${env.PATH}"
    } 

    // stage('Environment'){
    //     sh """
    //         echo project ${ocp_project}, secret ${pull_secret}, route ${public_route_prefix}, base domain ${ocp_base_domain}
    //         """
    // }   

    stage('Prepare'){
        withEnv(["CREDENTIALID=${nexus_credentials_id}"]) {
           withCredentials([[$class: 'UsernamePasswordMultiBinding',
                credentialsId: "${CREDENTIALID}",
                usernameVariable: 'nexus_username', passwordVariable: 'nexus_password']]) {
                    sh """
                        echo 'Downloading ci-cd templates...'
                        pwd
                        curl --fail -u ${nexus_username}:${nexus_password} -o cicd-template.tar.gz ${nexus_base_url}/repository/${nexus_cicd_repo}/cicd-template-${env_file}.tar.gz
                        rm -rf cicd-template
                        mkdir cicd-template && tar -xzvf ./cicd-template.tar.gz -C "\$(pwd)/cicd-template"
                        chmod -R 777 "\$(pwd)/cicd-template"
                        """
                    prepareSettingsXml(nexus_deps_repo, nexus_username, nexus_password, jenkins_local_repository)
                    addDistributionToPom(nexus_deploy_repo)
            }
        }

        pomappName = getFromPom('name')
        if(pomappName == null || pomappName.trim() == ""){
            pomappName = getFromPom('artifactId')
        }

        appFullVersion = getFromPom('version')
        gitCommitId = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
        echo "pomappName: '${pomappName}', appFullVersion:'${appFullVersion}', gitCommitId:'${gitCommitId}'"
    }

    stage('Java test') {
        sh """
        java -version
        """
    }

    stage('Build') {
        sh """
        mvn clean -U package -D skipTests -s ./cicd-template/maven/settings.xml
        """
    }

    stage ('Archive'){
        sh 'mvn deploy -P data-model -DskipTests -s ./cicd-template/maven/settings.xml'
    }
 
    // stage ('Clean') {
    //     sh """
    //         rm -rf *
    //         """
    // }

    stage('Docker image') {
        if (skipDockerBuild) {
            echo 'SKIP Docker image'
            Utils.markStageSkippedForConditional('Docker image')
        }
        else {
        sh """
        rm -f ./Dockerfile
        
        cp ./cicd-template/Dockerfiles/maven-slim.Dockerfile ./Dockerfile

        """
        jar_file = "${pomappName}-${appFullVersion}.jar" 
        sh """
            echo ${jar_file}
        """

        sh """
            docker build  -t ${nexus_docker_repo}/${pomappName} --build-arg JAR_FILE=${jar_file} .
            """
        sh """
            docker tag "${nexus_docker_repo}/${pomappName}" "${nexus_docker_url}/${nexus_docker_repo}/${pomappName}"
           """
        sh """
            docker image ls 
           """
        }
    }

    stage('Push image') {
        if (skipDockerPush) {
            echo 'SKIP Push image'
            Utils.markStageSkippedForConditional('Push image')
        }
        else {
            withEnv(["CREDENTIALID=${nexus_credentials_id}"]) {
                withCredentials([[$class: 'UsernamePasswordMultiBinding',
                credentialsId: "${CREDENTIALID}",
                usernameVariable: 'nexus_username', passwordVariable: 'nexus_password']]) { 

                sh """
                    docker login --username=${nexus_username} --password='${nexus_password}' ${nexus_docker_url}
                    docker push ${nexus_docker_url}/${nexus_docker_repo}/${pomappName}:latest
                    docker logout
                """

                    try {
                        sh """
                        set -e
                        set -x

                        docker rmi ${nexus_docker_repo}/${pomappName}:latest
                        docker rmi ${nexus_docker_url}/${nexus_docker_repo}/${pomappName}:latest
                        """
                    } catch (e) {
                        echo 'No docker images found' + e.toString()

                    }
                }
            }
        }
    }

    // stage('Yaml'){
    //     withEnv(["CREDENTIALID=${nexus_credentials_id}"]) {
    //         withCredentials([[$class: 'UsernamePasswordMultiBinding',
    //             credentialsId: "${CREDENTIALID}",
    //             usernameVariable: 'nexus_username', passwordVariable: 'nexus_password']]) { 
    //                 def  FILES_LIST = sh (script: "ls   './src/main/okd/'", returnStdout: true).trim()
    //                 //DEBUG
    //                 echo "FILES_LIST : ${FILES_LIST}"
    //                 //PARSING
    //                 for(String ele : FILES_LIST.split("\\r?\\n")){ 
    //                    println ">>>${ele}<<<"  
    //                    sh """
    //                     echo 'Uploading yaml(s) onto nexus repository...'
    //                     curl --fail -u ${nexus_username}:${nexus_password} --upload-file ./src/main/okd/${ele} $nexus_base_url/repository/cicd-projects/yaml/nobu-dev/$appName/${ele}
    //                     """
    //                 }
                    
    //             }
    //      }

    // }

    stage ('GKE Deploy'){
        echo resultlog
        if (resultlog.contains("gke")) {
            sh """
            gcloud auth activate-service-account --key-file=/opt/gcloud/${gke_keyfile}
            gcloud container clusters get-credentials ${gke_node} --region ${gke_region} --project ${gke_project}
            kubectl config set-context --current --namespace=${gke_namespace}
            """
            // sh "gcloud config set project ${gke_project}"
            // sh "kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "nexus-image-secret"}]}'"

            try {
                sh "kubectl get all -l app=${appName}-v1"
                sh "kubectl scale deployment ${appName}-v1 --replicas=0" 
            }catch (e) {
                echo 'No Deployment' + e.toString()
                //throw e
            }

            sh """ 
            kubectl apply -f ./src/main/gke/dev/service.yaml
            kubectl apply -f ./src/main/gke/dev/configmap.yaml
            kubectl apply -f ./src/main/gke/dev/deployment.yaml
            """
         }
        else 
        {
            echo 'SKIP GKE Deploy'
            Utils.markStageSkippedForConditional('GKE Deploy')
        }
    }

    stage ('Deploy Ingress'){
        if(resultlog.contains("ingress")){
            sh "kubectl apply -f ./src/main/gke/dev/ingress.yaml"
        } 
        else {
            echo 'SKIP Deploy Ingress'
            Utils.markStageSkippedForConditional('Deploy Ingress')
        }
    }

    // stage ('OpenShift Deploy'){
    //     echo resultlog
    //     if (resultlog.contains("okd")) {

    //         withEnv(["oc_credentials_id=${oc_credentials_id}","oc_login_url=${oc_login_url}"]) {
    //             withCredentials([[$class: 'UsernamePasswordMultiBinding',
    //                 credentialsId: "${oc_credentials_id}",
    //                 usernameVariable: 'oc_username', passwordVariable: 'oc_password']]) {
    //                     sh 'oc login -u=${oc_username} -p=${oc_password} ${oc_login_url} --insecure-skip-tls-verify=true'
    //                     sh 'oc project ${ocp_project}'
    //                     }
    //         }
    //         sh "oc scale dc ${appName}-v1 --replicas=0"
    //         sh "oc get all --selector app=${appName}-v1"
    //         sh "oc delete is --selector app=${appName}-v1"

    //         sh """ 
    //         oc apply -f ./src/main/okd/imagestream.yaml
    //         oc apply -f ./src/main/okd/configmap.yaml
    //         oc apply -f ./src/main/okd/deploymentconfig.yaml
    //         oc apply -f ./src/main/okd/service.yaml
    //         oc apply -f ./src/main/okd/route.yaml
    //         """
    //      }
    //     else 
    //     {
    //         echo 'SKIP OpenShift Deploy'
    //         Utils.markStageSkippedForConditional('OpenShift Deploy')
    //     }
    // }

}

def getFromPom(key) {
    // sh(returnStdout: true, script: "mvn dependency:resolve -U").trim();
    sh(returnStdout: true, script: "mvn -s ./cicd-template/maven/settings.xml -q -Dexec.executable=echo -Dexec.args='\${project.${key}}' --non-recursive exec:exec").trim()
}

def addDistributionToPom(nexus_deploy_repo) {
    pom = 'pom.xml'
    distMngtSection = readFile('./cicd-template/maven/pom-distribution-management.xml')
    distMngtSection = distMngtSection.replaceAll('\\$nexus_deploy_repo', nexus_deploy_repo)

    content = readFile(pom)
    newContent = content.substring(0, content.lastIndexOf('</project>')) + distMngtSection + '</project>'
    writeFile file: pom, text: newContent
}

def prepareSettingsXml(nexus_deps_repo, nexus_username, nexus_password, jenkins_local_repository) {
    settingsXML = readFile('./cicd-template/maven/settings.xml')
    settingsXML = settingsXML.replaceAll('\\$nexus_deps_repo', nexus_deps_repo)
    settingsXML = settingsXML.replaceAll('\\$nexus_username', nexus_username)
    settingsXML = settingsXML.replaceAll('\\$nexus_password', nexus_password)
    settingsXML = settingsXML.replaceAll('\\$local_repository', jenkins_local_repository)

    writeFile file: './cicd-template/maven/settings.xml', text: settingsXML
}